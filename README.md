# Admin Menu Swap

A Drupal module that allows you to provide a customized administration menu for
each role.

It is inspired by the [Administration Menu](https://www.drupal.org/project/admin_menu_source) module.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/amswap).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/amswap).


## Table of contents

- Requirements
- Installation
- Configuration
- Troubleshooting
- FAQ
- Maintainers


## Requirements

This module requires the following modules:

- menu


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules),
Ensure the Toolbar module is installed.


## Configuration

1. Go to Configuration > System > Admin Menu Swap to configure the role-menu 
   pairs.
2. Using Admin Menu Swap, you can choose which menu is displayed as the main 
   administrative menu (under "Manage") for each role. 


## Troubleshooting

- If you have any troubles, contact the maintainers directly.


## FAQ

 Q: Is this the same as the "Administration Menu Source" module?

 **A:** No, AMSwap was built from scratch for Drupal 8 by a separate developer,
 but it was inspired by Administration Menu Source.


## Maintainers

- Dane Rossenrode - [droces](https://www.drupal.org/u/droces), 
  founder and owner of [Touchdreams](https://touchdreams.co.za)
- Clayton Dewey - [cedewey](https://www.drupal.org/u/cedewey), 
  product owner at [DevCollaborative](https://devcollaborative.com)
